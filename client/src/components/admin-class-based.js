// CLASS BASED COMPONENT

import React from "react";
import { withOktaAuth } from "@okta/okta-react";

import { Card } from "./card";

// for info on the access token and authorization header functionality
// see...   https://developer.okta.com/docs/guides/sign-into-spa/react/use-the-access-token/

class ClassyAdmin extends React.Component{
  
  constructor(props){
    super(props);
    this.state = { userInfo: null, data: [] }; 

    this.baseUrl = `/posts`;

    this.checkUser = this.checkUser.bind(this);

    this.getPosts = this.getPosts.bind( this );
    this.addNewPost = this.addNewPost.bind( this );
    this.handleCancel = this.handleCancel.bind( this );
    this.handleEdit = this.handleEdit.bind( this );
    this.handleDelete = this.handleDelete.bind( this );
    this.handleSubmit = this.handleSubmit.bind( this );

    this.dataList = this.dataList.bind(this);
    this.mapData = this.mapData.bind(this);
    this.noDataDisclosure = this.noDataDisclosure.bind(this);
    
  }

  async componentDidMount(){    
    await this.checkUser();
    await this.getPosts();    
  }

  async componentDidUpdate(){
    await this.checkUser();
  }

  async checkUser(){
    if( this.props.authState.isAuthenticated && !this.state.userInfo ){
      const userInfo = await this.props.authService.getUser();
      this.setState( { userInfo: await userInfo } );
    }
  }

  noDataDisclosure(){
    let el = (
      <div className="card mt-5 col-sm">
        <div className="card-body">
          You don&rsquo;t have any posts. Use the &quot;Add New Post&quot; button to add some new posts.
        </div>
      </div>
    );
    return el;
  } 

  dataList(){
    return this.state.data.map( this.mapData );
  }

  mapData( item, idx ){
    
    return <Card 
      key={idx} 
      baselabel={"class"}
      item={item} 
      handleSubmit={ this.handleSubmit } 
      handleEdit={ this.handleEdit } 
      handleDelete={ this.handleDelete } 
      handleCancel={ this.handleCancel } 
    />;

  }

  getPosts(){
    // console.log("getPosts()");

    const accessToken = this.props.authState.accessToken;

    let getRequest = new Request( `${this.baseUrl}` );

    let getHeaders = new Headers();
    getHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    getHeaders.append( "Accept" , "application/json" );
    getHeaders.append( "Authorization" , `Bearer ${accessToken}` );

    const getoptionsobject = {
      method: "GET"
      , headers: getHeaders
    };
    
    return fetch( getRequest , getoptionsobject )
    .then( (response) => {
      return response.json();
    } )
    .then( ( data ) => {
      data.forEach( item => item.editMode = false );
      this.setState( { data: data } );
    } );

  }

  addNewPost(){
    // console.log("addNewPost()");
    let data = this.state.data;
    data.unshift( {
      editMode: true
      , title: ""
      , content: ""
    } );

    // console.log( "data", data );
    
    this.setState( { data: data } );
  }
  
  async handleCancel(){
    // await console.log("handleCancel()");
    await this.getPosts();
  }

  handleEdit( postId ){
    
    // console.log("handleEdit()");
    let data = this.state.data.map( ( item ) => {
      if( item.id === postId ){
        item.editMode = true;
      }
      return item;
    } );

    this.setState( { data: data } );
  }

  async handleDelete( postId ){

    // await console.log("handleDelete()");

    const accessToken = this.props.authState.accessToken;

    let deleteRequest = new Request( `${this.baseUrl}/${postId}` );

    let deleteHeaders = new Headers();
    deleteHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    deleteHeaders.append( "Accept" , "application/json" );
    deleteHeaders.append( "Authorization" , `Bearer ${accessToken}` );

    const deleteoptionsobject = {
      method: "DELETE"
      , headers: deleteHeaders
    };

    return fetch( deleteRequest , deleteoptionsobject )
    .then( (res) => {
      // console.log( "handleDelete() res", res );
      return this.getPosts();
    } ); 
  }

  async handleSubmit( event ){
    
    event.preventDefault();
    // await console.log("handleSubmit()");

    const data = new FormData();

    if( document.querySelector("#id") !== null ){
      // console.log( 'document.querySelector("#id").value' , document.querySelector("#id").value );
      data.append("id" , parseInt( document.querySelector("#id").value ) );
    }
    data.append("title" , document.querySelector("#title").value );
    data.append("content" , document.querySelector("#content").value );

    const accessToken = this.props.authState.accessToken;

    let submitHeaders = new Headers();
    submitHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    submitHeaders.append( "Accept" , "application/json" );
    submitHeaders.append( "Authorization" , `Bearer ${accessToken}` );
    
    if( Number.isSafeInteger( parseInt( data.get("id") ) ) ){

      let ifSubmitRequest = new Request( `${this.baseUrl}/${ data.get("id") }` );

      const body = JSON.stringify( {
        id: parseInt( data.get("id") )
        , title: data.get("title")
        , content: data.get("content")
      } ); 
      
      const putoptionsobject = {
        method: "PUT"
        , headers: submitHeaders
        , body: body
      };

      return fetch( ifSubmitRequest , putoptionsobject )
      .then( (res) => {
        // console.log( "handleSubmit() put res", res );
        return this.getPosts();
      } ); 
    }
    else{

      let elseSubmitRequest = new Request( this.baseUrl );

      const body = JSON.stringify( {
        title: data.get("title")
        , content: data.get("content")
      } );

      const postoptionsobject = {
        method: "POST"
        , headers: submitHeaders
        , body: body
      };

      return fetch( elseSubmitRequest , postoptionsobject )
      .then( (res) => {
        // console.log( "handleSubmit() post res", res );
        return this.getPosts();
      } ); 
    }

  }

  render(){
    
    const el = (
      <>
        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">
              
              <h1>Admin!</h1>
              
              <div className="card mt-4">

                <div className="card-body">
                  
                  {/* Konichiwa!!! */}

                  { this.props.authState.isPending && (<p>Your request is pending and loading&hellip;</p>) }
                  
                  { this.state.userInfo !== null && (<p>Welcome back, { this.state.userInfo.name }!</p>) }

                </div>
                
              </div>

            </div>
          </div>

          
          <div className="row">
            <div className="col-md-6 mx-auto">
              
              {/* <h1>ADMIN!</h1> */}

              <button type="button" className="mt-4 mb-2 btn btn-primary btn-sm float-right" onClick={ this.addNewPost }>Add New Post</button>

              { this.state.data.length === 0 && this.noDataDisclosure() }

              { this.state.data.length > 0 && this.dataList() }

            </div>
          </div>
          
        </div>
      </>
    );
  
    return el;

  }

}

// export {ClassyAdmin};
const Admin = withOktaAuth( ClassyAdmin );
export { Admin };

/*
FROM [ https://developer.okta.com/docs/guides/sign-into-spa/react/user-info/ ]
SEE [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react ]

For class-based components, authState and authService are passed as props to your component via the withOktaAuth higher-order component. [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#withoktaauth ]

Your code can get the user's profile using the getUser() method [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservicegetuser ] on the AuthService object [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservice ]. You should wait until the authState.isAuthenticated flag is true.

*/