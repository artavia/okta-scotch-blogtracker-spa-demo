// CLASS BASED COMPONENT

import React from "react";
import { NavLink /* , useHistory */ } from "react-router-dom";

import { withOktaAuth } from "@okta/okta-react";

import { CardReadOnly } from "./card-read-only";

class ClassyHome extends React.Component{
  
  constructor(props){
    super(props);

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);

    this.getPosts = this.getPosts.bind( this );
    this.state = { data: [] }; 

    this.baseUrl = `/blogpreview`;

    this.dataList = this.dataList.bind(this);
    this.mapData = this.mapData.bind(this);
    this.noDataDisclosure = this.noDataDisclosure.bind(this);
  }

  async componentDidMount(){ 
    await this.getPosts();    
  }

  getPosts(){
    // console.log("getPosts()");

    let getRequest = new Request( `${this.baseUrl}` );

    let getHeaders = new Headers();
    getHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    getHeaders.append( "Accept" , "application/json" );

    const getoptionsobject = {
      method: "GET"
      , headers: getHeaders
    };
    
    return fetch( getRequest , getoptionsobject )
    .then( (response) => {
      return response.json();
    } )
    .then( ( data ) => {
      const { posts } = data;
      this.setState( { data: posts } );
    } );

  }

  noDataDisclosure(){
    let el = (
      <div className="card mt-5 col-sm">
        <div className="card-body">
          You don&rsquo;t have any posts. Use the &quot;Add New Post&quot; button to add some new posts.
        </div>
      </div>
    );
    return el;
  } 

  dataList(){
    return this.state.data.map( this.mapData );
  }

  mapData( item, idx ){
    
    return <CardReadOnly 
      key={idx} 
      item={item} 
    />;

  }

  async login(event){
    event.preventDefault();
    // await this.props.authService.login("/profile");
    await this.props.authService.login("/admin");
  }

  async logout(event){
    event.preventDefault();
    await this.props.authService.logout("/"); // uri parameter is optional

    // CULL THE COOKIES
    // care of... https://stackoverflow.com/questions/179355/clearing-all-cookies-with-javascript
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

    // CULL ANY LOCALSTORAGE
    window.localStorage.clear();
  }

  elPending(){
    return (
      <>
        <div className="card-body">
          {/* Konichiwa!!! */}

          Your request is pending and loading&hellip;
        </div>
      </>
    );
  }

  elNotAuthenticated(){
    return (
      <>
        <div className="card-body">
          
          {/* Konichiwa!!! */}

          <h2>You are not signed in yet.</h2>
          <p>By all means please login!</p>

          <NavLink role="button" to={""} className="btn btn-outline-success navbar-btn my-2 my-sm-0 mr-sm-2" onClick={this.login}>
            Login
          </NavLink>

        </div>
      </>
    );
  }

  elAuthenticated(){
    return (
      <>
        <div className="card-body">
          
          {/* Konichiwa!!! */}

          <h2>You are authenticated, baby!!!</h2>

          <NavLink role="button" to={""} className="btn btn-outline-danger navbar-btn my-2 my-sm-0 mr-sm-2" onClick={this.logout}>
            Logout
          </NavLink>

        </div>
      </>
    );
  }

  render(){
    
    const el = (
      <>
        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">
              
              <h1 className="mt-1">HOME!</h1>
              
              <div className="card mt-4">
                
                { !this.props.authState.isAuthenticated && this.elNotAuthenticated() }

                { this.props.authState.isAuthenticated && this.elAuthenticated() }

                { this.props.authState.isPending && this.elPending() }
                
              </div>

            </div>
          </div>

          <div className="row">
            <div className="col-md-6 mx-auto">
              
              <h3 className="mt-2">YOUR BLOG POSTS!</h3>

              { this.state.data.length === 0 && this.noDataDisclosure() }

              { this.state.data.length > 0 && this.dataList() }

            </div>
          </div>

        </div>
      </>
    );
  
    return el;

  }

}

// export { ClassyHome };
const Home = withOktaAuth( ClassyHome );
export { Home };

/*
FROM [ https://developer.okta.com/docs/guides/sign-into-spa/react/add-signin-button/ ]

Class-based components can use the withOktaAuth higher-order component to receive the authService and authState objects as props.

The okta-react SDK provides an authState object that provides information on the state of the current user's authentication. You can use the authState.isAuthenticated property, for example, to show or hide a button depending on whether the user is signed in.

The okta-react SDK provides an authService object that provides methods to read more details about, or to modify, the current authentication. The authService.login() method lets you specify the path you'd like the user to be navigated to after authenticating.

*/