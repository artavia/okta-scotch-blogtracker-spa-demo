import React from 'react';

const Card = ( { item , baselabel , handleSubmit, handleEdit, handleDelete, handleCancel  } ) => {
  
  const { id, title, content, editMode } = item;
  
  // console.log("\n\n");
  // console.log( "id" , id );
  // console.log( "Number.isSafeInteger( parseInt( id ) )" , Number.isSafeInteger( parseInt( id ) ) );
  // console.log( "editMode" , editMode );
  // console.log( "baselabel" , baselabel );
  // console.log( "Array.isArray(item)" , Array.isArray(item) );
  // console.log( "typeof( item )" , typeof( item ) );
  // console.log( "item" , item );
  // console.log("\n");
  
  if( (editMode === true) && ( baselabel === "class" || baselabel === "function") ){

    // console.log( "id" , id );

    let elEdit = (
      <>
        <div className="card mt-4">
          <div className="card-body">
            
            <form onSubmit={ handleSubmit } >

              { id !== undefined && (<><input type="hidden" name="id" id="id" value={ id } /></>) }
              
              <div className="input-group input-group-sm mb-3">
                <label htmlFor="title" className="form-check-label">Title:</label>
                <input type="text" name="title" id="title" className="form-control" placeholder="Title" defaultValue={ title }  />
              </div>
              
              <div className="input-group input-group-sm mb-3">
                <label htmlFor="content" className="form-check-label">Content:</label>
                <textarea name="content" id="content" className="form-control" placeholder="Content" defaultValue={ content }  ></textarea>
              </div>

              <button type="button" className="btn btn-outline-secondary btn-sm" onClick={ handleCancel }>Cancel</button>
              <button type="submit" className="btn btn-info btn-sm ml-2" >Save</button>

            </form>

          </div>
        </div>
      </>
    );

    return elEdit;

  }
  // else
  if( (editMode === false) && ( baselabel === "class" ) ){
    let elNoEdit = (
      <>
        <div className="card mt-4">
          <div className="card-body">
            <h5 className="card-title">
              { title || "No Title" }
            </h5>
            <p className="card-text">
              { content || "No Content"}
            </p>
            <button type="button" className="btn btn-outline-danger btn-sm" onClick={ () => handleDelete( id ) }>Delete</button>
            <button className="btn btn-info btn-sm ml-2" type="submit" onClick={ () => handleEdit(id) }>Edit</button>
          </div>
        </div>
      </>
    ); 
    return elNoEdit;
  }
  // else
  if( ( id === undefined ) && ( Number.isSafeInteger( parseInt( id ) ) === false ) && ( editMode === false ) && ( baselabel = "function" ) && ( Array.isArray(item) === true ) ){
    let elNoEdit = (
      <>
        <div className="card mt-4">
          <div className="card-body">
            <h5 className="card-title">
              { item[0].title || "No Title GOTCHA" }
            </h5>
            <p className="card-text">
              { item[0].content || "No Content GOTCHA"}
            </p>
            <button type="button" className="btn btn-outline-danger btn-sm" onClick={ () => handleDelete( id ) }>Delete</button>
            <button className="btn btn-info btn-sm ml-2" type="submit" onClick={ () => handleEdit(id) }>Edit</button>
          </div>
        </div>
      </>
    ); 
    return elNoEdit;
  }
  // else
  if( (editMode === false) && ( baselabel === "function" ) ){
    let elNoEdit = (
      <>
        <div className="card mt-4">
          <div className="card-body">
            <h5 className="card-title">
              { title || "No Title" }
            </h5>
            <p className="card-text">
              { content || "No Content"}
            </p>
            <button type="button" className="btn btn-outline-danger btn-sm" onClick={ () => handleDelete( id ) }>Delete</button>
            <button className="btn btn-info btn-sm ml-2" type="submit" onClick={ () => handleEdit(id) }>Edit</button>
          </div>
        </div>
      </>
    ); 
    return elNoEdit;
  }  

};

export { Card };