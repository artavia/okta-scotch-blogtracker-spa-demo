// FUNCTION BASED COMPONENT

import React from "react";

import { useState, useEffect, useCallback } from "react";

import { useOktaAuth } from "@okta/okta-react";

import { Card } from "./card";

// for info on the access token and authorization header functionality
// see...   https://developer.okta.com/docs/guides/sign-into-spa/react/use-the-access-token/

// e.g. const accessToken = authState.accessToken;

const Admin = () => {

  const baseUrl = `/posts`;

  const [ userInfo, setUserInfo ] = useState(null);
  const [ data, setData ] = useState([]); 

  const { authState , authService } = useOktaAuth();

  const checkUser = useCallback( () => {
    
    // console.log( ">>>>> authState.isAuthenticated \n\n " , authState.isAuthenticated );
    // console.log( ">>>>> userInfo \n\n " , userInfo );
    
    // if user IS NOT authenticated, set userInfo to null
    if( !authState.isAuthenticated ){
      setUserInfo( null );
    }
    // if user IS authenticated, set userInfo to the response
    else {
      authService.getUser()
      .then( async (info) => {
        // console.log( ">>>>> await info \n\n " , await info );
        await setUserInfo( info );
      } );
    }
  } , [ authState, authService ] ); // Update if authState changes

  const postsInnerFunction = useCallback( async () => {

    const abortController = await new AbortController();
    const signal = await abortController.signal;

    const accessToken = authState.accessToken;

    let getRequest = new Request( `${baseUrl}`, { signal: await signal } );

    let getHeaders = new Headers();
    getHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    getHeaders.append( "Accept" , "application/json" );
    getHeaders.append( "Authorization" , `Bearer ${accessToken}` );

    const getoptionsobject = {
      method: "GET"
      , headers: getHeaders
    };

    const alldatapromise = await fetch( getRequest , getoptionsobject )
    .then( response => {
      return response.json();
    } )
    .then( (data) => { 
      // console.log( "thennable data promise..." , data );
      // return data;
      data.forEach( item => item.editMode = false );
      return data;
    } )
    .catch( (err) => {
      console.log( "\n\n await alldatapromise err \n\n", err ); 
      
      if ( err.name !== "AbortError" ) {
        console.log( "\n\n FETCH err \n\n", err );
      } 

    } );

    // console.log( "alldatapromise" , alldatapromise );

    if( alldatapromise !== undefined ){
      try{ 
        setData( alldatapromise );
        return () => {
          signal.abortController();
        };
      }
      catch( err ){
        console.log( "\n\n >>>> alldatapromise === undefined... err \n\n", err );
        // setError( err.message );
      }
    }

  } , [ authState.accessToken , baseUrl ] ); // Update if VARIABLES change

  useEffect( () => { 
    checkUser();
    postsInnerFunction();
    // return () => {};
  } , [ checkUser , postsInnerFunction ] ); // Update 

  const noDataDisclosure = () => {
    let el = (
      <div className="card mt-5 col-sm">
        <div className="card-body">
          You don&rsquo;t have any posts. Use the &quot;Add New Post&quot; button to add some new posts.
        </div>
      </div>
    );
    return el;
  };

  const dataList = () => {
    return data.map( mapData );
  };

  const mapData = (item,idx) => {
    return <Card 
      key={idx} 
      baselabel={"function"}
      item={item} 
      handleSubmit={ handleSubmit } 
      handleEdit={ handleEdit } 
      handleDelete={ handleDelete } 
      handleCancel={ handleCancel } 
    />;
  };

  function getPosts(){
    
    // console.log("getPosts useCallback");

    const accessToken = authState.accessToken;

    let getRequest = new Request( "/posts" );

    let getHeaders = new Headers();
    getHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    getHeaders.append( "Accept" , "application/json" );
    getHeaders.append( "Authorization" , `Bearer ${accessToken}` );

    const getoptionsobject = {
      method: "GET"
      , headers: getHeaders
    }; 
    
    return fetch( getRequest , getoptionsobject )
    .then( (response) => {
      return response.json();
    } )
    .then( ( data ) => {
      data.forEach( item => item.editMode = false );
      setData( data ); 
    } ); 
  }

  // with useState the variables are READ-ONLY and while the REFERENCE DOES NOT change problems will ensue... ergot:
  // see https://medium.com/javascript-in-plain-english/how-to-deep-copy-objects-and-arrays-in-javascript-7c911359b089
  
  // try with ... spread operator ~ E.G.: var newArray = [...oldArray]; 
  // try with ... slice() method ~ E.G.:  var newArray = oldArray.slice(); 
  // try with ... Object.assign() ~ E.G.: Object.assign(target, source)  >>> // Object.assign([], oldArray)

  // OR... just MAP the DARN thing!!!

  const addNewPost = () => {
    // console.log("addNewPost()"); 

    // let data = data; // BUU!!! ELICITS >>> ReferenceError: can't access lexical declaration 'data' before initialization

    let copy = data; // console.log("\n\n >>>>> data \n\n" , copy ); 

    let mapped = copy.map( ( item, idx, arr ) => {

      const newArray = [];

      if( idx <= arr.length -1 ){
        newArray.push( item );
      }

      return newArray;

    } );

    mapped.forEach( item => item.editMode = false ); // console.log( "UNO mapped", mapped );
    
    let newArr = Object.assign([], mapped ); // console.log( "DOS newArr", newArr );
    
    newArr.unshift( {
      editMode: true
      , title: ""
      , content: ""
    } );

    // console.log( "newArr", newArr );

    setData( newArr );
  }; 

  const handleCancel = async () => {
    // await console.log("handleCancel()");
    await getPosts();
  };

  const handleEdit = ( postId ) => {

    // console.log("handleEdit()");
    
    let copy = data.map( ( item ) => {
      if( item.id === postId ){
        item.editMode = true;
      }
      return item;
    } );

    setData( copy );

  };

  const handleDelete = async ( postId ) => {

    // await console.log("handleDelete()");
    
    const accessToken = authState.accessToken;
    
    let deleteRequest = new Request( `${baseUrl}/${postId}` );

    let deleteHeaders = new Headers();
    deleteHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    deleteHeaders.append( "Accept" , "application/json" );
    deleteHeaders.append( "Authorization" , `Bearer ${accessToken}` );

    const deleteoptionsobject = {
      method: "DELETE"
      , headers: deleteHeaders
    };

    return fetch( deleteRequest , deleteoptionsobject )
    .then( (res) => {
      // console.log( "handleDelete() res", res );
      // return 
      getPosts();
    } ); 

  };

  const handleSubmit = async ( event ) => {
    
    event.preventDefault();
    // await console.log("handleSubmit()");
    
    const data = new FormData();

    if( document.querySelector("#id") !== null ){
      // console.log( 'document.querySelector("#id").value' , document.querySelector("#id").value );
      data.append("id" , parseInt( document.querySelector("#id").value ) );
    }
    data.append("title" , document.querySelector("#title").value );
    data.append("content" , document.querySelector("#content").value );

    const accessToken = authState.accessToken;

    let submitHeaders = new Headers();
    submitHeaders.append( "Content-type" , "application/json; charset=UTF-8" );
    submitHeaders.append( "Accept" , "application/json" );
    submitHeaders.append( "Authorization" , `Bearer ${accessToken}` );
    
    if( Number.isSafeInteger( parseInt( data.get("id") ) ) ){

      let ifSubmitRequest = new Request( `${baseUrl}/${ data.get("id") }` );

      const body = JSON.stringify( {
        id: parseInt( data.get("id") )
        , title: data.get("title")
        , content: data.get("content")
      } ); 
      
      const putoptionsobject = {
        method: "PUT"
        , headers: submitHeaders
        , body: body
      };

      return fetch( ifSubmitRequest , putoptionsobject )
      .then( (res) => {
        // console.log( "handleSubmit() put res", res );
        // return 
        getPosts();
      } ); 
    }
    else{

      let elseSubmitRequest = new Request( baseUrl );

      const body = JSON.stringify( {
        title: data.get("title")
        , content: data.get("content")
      } );

      const postoptionsobject = {
        method: "POST"
        , headers: submitHeaders
        , body: body
      };

      return fetch( elseSubmitRequest , postoptionsobject )
      .then( (res) => {
        // console.log( "handleSubmit() post res", res );
        // return 
        getPosts();
      } ); 
    }
    
  };
  
  const el = (
    <>
      <div className="py-5">

        <div className="row">
          <div className="col-md-6 mx-auto">
            
            <h1>Admin!</h1>
            
            <div className="card mt-4">

              <div className="card-body">

                {/* Konichiwa!!! */}
                
                { authState.isPending && ( <p>Your request is pending and loading&hellip;</p> ) }

                { userInfo !== null && (<p>Welcome back, { userInfo.name }!</p>) }

              </div>
              
            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-6 mx-auto">
            
            {/* <h1>ADMIN!</h1> */}

            <button type="button" className="mt-4 mb-2 btn btn-primary btn-sm float-right" onClick={ addNewPost }>Add New Post</button>

            { data.length === 0 && noDataDisclosure() }

            { data.length > 0 && dataList() }

          </div>
        </div>

      </div>
    </>
  );

  return el;
};

export { Admin };

/*
FROM [ https://developer.okta.com/docs/guides/sign-into-spa/react/user-info/ ]
SEE [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react ]

For function-based components, authState and authService are returned by the useOktaAuth hook. [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#useoktaauth ]

Your code can get the user's profile using the getUser() method [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservicegetuser ] on the AuthService object [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservice ]. You should wait until the authState.isAuthenticated flag is true.

*/