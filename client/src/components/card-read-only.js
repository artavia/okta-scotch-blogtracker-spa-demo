import React from 'react';

const CardReadOnly = ( { item } ) => {

  const { title, content } = item;
  
  // console.log( "item", item ); 
  // console.log( "title", title );
  // console.log( "content", content );

  let elNoEdit = (
    <>
      <div className="card mt-4">
        <div className="card-body">
          <h5 className="card-title">
            { title }
          </h5>
          <p className="card-text">
            { content }
          </p>
        </div>
      </div>
    </>
  );
  
  return elNoEdit;

};

export { CardReadOnly };