import React from "react";

import {
  REACT_APP_OKTA_ORG_URL
  , REACT_APP_OKTA_CLIENT_ID
  , REACT_APP_REDIRECT_URI 
  , REACT_APP_POSTLOGOUT_REDIRECT_URI 
  , REACT_APP_REDIRECT_PATH
  , NODE_ENV
} from "../custom-okta-security-configuration/development-environment-variables";

class AlphaTest extends React.Component{
  
  // constructor(props){
  //   super(props);
  // }

  render(){

    // const { NODE_ENV } = process.env;
    
    const el = (
      <>
        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">
              
              <div className="card mt-4">
                <div className="card-body">
                  
                  <small>You are running this application in <b>{NODE_ENV}</b> mode.</small>
                  <form>
                    
                    <input type="text" className="form-control" defaultValue={REACT_APP_OKTA_ORG_URL} /> <br />
                    <input type="text" className="form-control" defaultValue={REACT_APP_OKTA_CLIENT_ID} /> <br />
                    
                    <input type="text" className="form-control" defaultValue={REACT_APP_REDIRECT_URI} /> <br />
                    <input type="text" className="form-control" defaultValue={REACT_APP_POSTLOGOUT_REDIRECT_URI} /> <br />
                    <input type="text" className="form-control" defaultValue={REACT_APP_REDIRECT_PATH} /> <br />
                  </form>
                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </>
    );
  
    return el;

  }

}

export { AlphaTest };
