import React from "react";

import { Alpha } from "./layout/alpha";

const App = () => {
  
  const el = (
    <>
      <Alpha />
    </>
  ); 

  return el;
}

export { App };
