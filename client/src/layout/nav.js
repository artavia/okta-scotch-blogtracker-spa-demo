import React from "react";
import { NavLink } from "react-router-dom";

import { useOktaAuth } from "@okta/okta-react";

const Nav = () => {

  const { authState , authService } = useOktaAuth();

  const noOpLink = ( event ) => { event.preventDefault(); };

  const login = (event) => {
    event.preventDefault(); 
    authService.login("/admin");
  };

  const logout = (event) => {
    event.preventDefault();
    
    // return 
    authService.logout("/"); // uri parameter is optional

    // CULL THE COOKIES
    // care of... https://stackoverflow.com/questions/179355/clearing-all-cookies-with-javascript
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

    // CULL ANY LOCALSTORAGE
    window.localStorage.clear();

  };
  
  const el = (
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      
      <NavLink className="navbar-brand" to={"/"} onClick={ noOpLink }>Blog App</NavLink>
      
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarsExampleDefault">

        <ul className="navbar-nav mr-auto">
          
          <li className="nav-item">
            <NavLink className="nav-link" to={"/"} activeClassName={"active"} exact>
              Home
            </NavLink>
          </li>

          { authState.isAuthenticated  && <> 
            <li className="nav-item">
              <NavLink className="nav-link" to={"/admin"} activeClassName={"active"} exact>
                Admin
              </NavLink>
            </li> 
          </> }

          {/* <li className="nav-item">
            <NavLink className="nav-link" to={"/moarrtest"} activeClassName={"active"} exact>
              MOARRR TEST!!!
            </NavLink>
          </li> */}

          { authState.isAuthenticated  && <> 

            <li className="nav-item">
              <NavLink className="nav-link" to={"/alphatest"} activeClassName={"active"} exact>
                Alpha Test
              </NavLink>
            </li>
            
          </> }

        </ul>

        <ul className="navbar-nav">

          { authState.isAuthenticated  && <> 
            <li className="nav-item">
              
              <NavLink role="button" className="btn btn-outline-danger navbar-btn my-2 my-sm-0 mr-sm-2" to={""} activeClassName={"active"} exact onClick={ logout }>
                Logout
              </NavLink>

            </li>
          </> }
          
          { !authState.isAuthenticated  && <> 
            <li className="nav-item">
              
              <NavLink role="button" className="btn btn-outline-success navbar-btn my-2 my-sm-0 mr-sm-2" to={""} activeClassName={"active"} exact onClick={ login }>
                Login
              </NavLink>

            </li>
          </> }

        </ul>

      </div>

    </nav>
  );

  return el;
}

export { Nav };
