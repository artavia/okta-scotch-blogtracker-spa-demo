import React from "react";

const Footer = () => {
  
  const el = (
    <footer className="container">
      
      <div className="starter-template">
        <p>
          &copy; <a href="https://github.com/donLucho?tab=repositories" target="_blank" rel="noopener noreferrer">GitHub repositories</a> | <a href="https://gitlab.com/users/artavia/projects" target="_blank" rel="noopener noreferrer">GitLab projects</a>
        </p>
      </div>

    </footer>
  );

  return el;
}

export { Footer };
