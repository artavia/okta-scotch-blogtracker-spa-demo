const { 
  REACT_APP_OKTA_ORG_URL
  , REACT_APP_OKTA_CLIENT_ID
  
  // , REACT_APP_REDIRECT_URI    // returned "http://localhost:3000/implicit/callback" 
  , REACT_APP_REDIRECT_URI = `${window.location.origin}/implicit/callback`
  // , REACT_APP_REDIRECT_URI = typeof window !== "undefined" && `${window.location.origin}/implicit/callback`

  // , REACT_APP_POSTLOGOUT_REDIRECT_URI  // returned "http://localhost:3000/"
  , REACT_APP_POSTLOGOUT_REDIRECT_URI = `${window.location.origin}/`
  // , REACT_APP_POSTLOGOUT_REDIRECT_URI = typeof window !== "undefined" && `${window.location.origin}/`

  , REACT_APP_REDIRECT_PATH 

  , NODE_ENV
} = process.env;

export { 
  REACT_APP_OKTA_ORG_URL
  , REACT_APP_OKTA_CLIENT_ID
  , REACT_APP_REDIRECT_URI 
  , REACT_APP_POSTLOGOUT_REDIRECT_URI 
  , REACT_APP_REDIRECT_PATH 

  , NODE_ENV
};