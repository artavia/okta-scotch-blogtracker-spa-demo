import { 
  REACT_APP_OKTA_ORG_URL
  , REACT_APP_OKTA_CLIENT_ID
  , REACT_APP_REDIRECT_URI 
  , REACT_APP_POSTLOGOUT_REDIRECT_URI 
} from "./development-environment-variables";

const securityConfigurationObject = {
  clientId: REACT_APP_OKTA_CLIENT_ID
  , issuer: `${REACT_APP_OKTA_ORG_URL}/oauth2/default`
  , redirectUri: REACT_APP_REDIRECT_URI
  , scopes: [ "openid", "profile", "email" ]
  , pkce: true
  , postLogoutRedirectUri: REACT_APP_POSTLOGOUT_REDIRECT_URI
};

export { securityConfigurationObject };