if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { 
  PORT
} = process.env;

const restify = require("restify");

const server = restify.createServer(
  {
    name: 'myrestifyapp'
    , version: '1.0.0' 
  }
); // http://restify.com/docs/server-api/#createserver

server.use( restify.plugins.acceptParser( server.acceptable) );
server.use( restify.plugins.queryParser() );
server.use( restify.plugins.bodyParser() );

server.get( '/echo/:name' , (req,res,next) => { 
  
  // res.send( req.params ); // RETURNS { "name": "defeatedfoe" }

  const { name } = req.params;
  res.contentType = 'json'; // SEE  http://restify.com/docs/response-api/#json
  res.send( { message: name } );
  return next();
} ); 
// TEST WITH  http://127.0.0.1:5000/echo/defeatedfoe

server.listen( PORT, () => {
  console.log( '%s listening at %s', server.name, server.url )
} );