( () => {

  const sequelize = require("./../custom_db/test-database-obj");

  // DB CONNECTION 
  // =============================================
  const connectSqlite = ( sequelize ) => {
    
    console.log( "==>>>> Database connection in progress...\n\n" );

    // =============================================
    // TEST THE CONNECTION ~ AUTHENTICATION PROMISE
    // https://sequelize.org/master/manual/getting-started.html#testing-the-connection
    // =============================================
    
    return sequelize.authenticate()
    .then( () => {
      console.log("Connection has been successfully established.\n\n");
    } )
    .catch( (derrp) => {
      console.error("Unable to connect to DB: \n\n" , derrp );
      throw derrp;
    } );

  };

  // EXPORT FUNCTION 
  // =============================================
  const asyncHandling = async () => {
    let database = await Promise.resolve( connectSqlite( sequelize ) );
    return database;
  }; 

  // MODULE EXPORT DECLARATION
  // =============================================
  module.exports = asyncHandling;
  
} )();