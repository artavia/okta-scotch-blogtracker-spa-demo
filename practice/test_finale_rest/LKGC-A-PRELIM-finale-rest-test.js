if( process.env.NODE_ENV !== "production" ){ 
  const dotenv = require("dotenv"); 
  dotenv.config();
}

const { 
  NODE_ENV
  , PORT
  , HOSTNAME
  , USE_RESTIFY
} = process.env;

const { 
  Sequelize , DataTypes
  // , Op 
} = require("sequelize");

const finale = require("finale-rest");
// const ForbiddenError = require("finale-rest").Errors.ForbiddenError;

const http = require("http");
const path = require("path");

const database = new Sequelize( {
  dialect: "sqlite"
  // , storage: "./teststooges.sqlite3"
  // , storage: path.join( __dirname, "databases", "teststooges.sqlite3" )
  , storage: path.join( "./", "databases", "teststooges.sqlite3" )
} );

// const connectDB = require("./../custom_auth_promise/test-authentication");

const User = database.define( "users" , {
  username: {
    type: DataTypes.STRING
    , allowNull: false
  }
  , birthday: {
    type: DataTypes.DATE
  }
  , tagline: {
    type: DataTypes.STRING
    , allowNull: false
  }
} );

// const User = require("./../custom_models_sequelize/user.test-model");

// Initialize server
var server;
var app;

if( USE_RESTIFY ) {
  
  const restify = require("restify");
  const corsMiddleware = require("restify-cors-middleware");

  app = server = restify.createServer();
  
  const corsOptions = {
    preflightMaxAge: 5
    , origins: ["*"]
    , allowHeaders: [ "Authorization", "API-Token" , "Content-Range" ]
    , exposeHeaders: [ "Authorization" , "API-Token-Expiry" , "Content-Range" ]
  };
  var cors = corsMiddleware( corsOptions );

  server.pre( cors.preflight );
  server.use( cors.actual );

  server.use( restify.plugins.queryParser() );
  server.use( restify.plugins.bodyParser() );
  server.use( restify.plugins.acceptParser( server.acceptable ) );

}
else {
  const express = require("express");
  const app = express();

  // const bodyParser = require("body-parser");
  // app.use( bodyParser.json() );
  // app.use( bodyParser.urlencoded( { extended: false } ) );
  // app.use( bodyParser.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded  

  app.use( express.json() ); // for parsing application/json
  app.use( express.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded

  server = http.createServer( app );
}

// Initialize finale
finale.initialize( {
  app: app
  , sequelize: database // connectDB()
} );

// Create REST resource
const resourceConfig = {
  model: User
  , endpoints: [ "/users" , "/users/:id" ]
};
const userResource = finale.resource( resourceConfig );

// Create database and listen
// connectDB()
database
.sync()
.then( async () => {

  await app.listen( PORT, HOSTNAME, () => {
    console.log( `http server running at http://${HOSTNAME}:${PORT}/` );
  } );

} );

/*
On the server we now have the following controllers and endpoints:
Controller  Endpoint  Description
userResource.create   POST /users   Create a user
userResource.list   GET /users  Get a listing of users
userResource.read   GET /users/:id  Get details about a user
userResource.update   PUT /users/:id  Update a user
userResource.delete   DELETE /users/:id   Delete a user
*/