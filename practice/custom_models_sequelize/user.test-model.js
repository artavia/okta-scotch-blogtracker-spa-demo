const sequelize = require("./../custom_db/test-database-obj")

const { DataTypes } = require("sequelize");
// const { Sequelize } = require("sequelize");

console.log( "Let's get this show on the road!!!\n\n" );

// =============================================
// MODEL CREATION PHASE FOR MAPPING
// https://sequelize.org/master/manual/model-basics.html#model-definition
// =============================================

const User = sequelize.define( "users" , {
  username: {
    type: DataTypes.STRING
    , allowNull: false
  }
  , birthday: {
    type: DataTypes.DATE
  }
  , tagline: {
    type: DataTypes.STRING
    , allowNull: false
  }
} );

module.exports = User;