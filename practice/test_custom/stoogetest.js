if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { 
  PORT
  , HOSTNAME
} = process.env;

const { Op } = require("sequelize");
const restify = require("restify");

const connectDB = require("./../custom_auth_promise/test-authentication");
const User = require("./../custom_models_sequelize/user.test-model");

const app = restify.createServer(
  {
    name: 'myrestifyapp' // [string]
    , version: '1.0.0' // [string]
  }
); // http://restify.com/docs/server-api/#createserver

app.use( restify.plugins.acceptParser( app.acceptable) );
app.use( restify.plugins.queryParser() );
app.use( restify.plugins.bodyParser() );

app.get( "/users" , ( req, res, next ) => {
  
  // res.contentType = 'json'; // SEE  http://restify.com/docs/response-api/#json
  // res.send( { message: "This is the Users ENDPOINT." } );

  // TODO: retrieve all records
    
  User.findAll()
  .then( users => {
    
    console.log( '>>>>>> All record: ' , users );
    // res.status(200).json( users );

    res.contentType = 'json'; // SEE  http://restify.com/docs/response-api/#json
    res.send( { users } );
    return next();

  } )
  .catch( ( err ) => {
    // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    // res.status(400).json( { errormessage: "Internal Error -- problem loading data." } ); 

    res.contentType = 'json'; // SEE  http://restify.com/docs/response-api/#json
    res.send( { errormessage: "Internal Error -- problem loading data." } );

  } );

  // TEST WITH...
  // http://127.0.0.1:5000/users

} );

app.get( "/users/:id" , ( req, res, next ) => {

  const id = parseInt( req.params.id );

  // TODO: retrieve one record using WHERE modifier

  // User.findAll( // version one 
  //   { 
  //     where: {
  //       id: id
  //     } 
  //   }
  // ) 
  User.findAll( // version two
    { 
      where: {
        id: {
          [Op.eq]: id
        }
      } 
    }
  )
  .then( users => {
    console.log( '>>>>>> One record: ' , users );
    // res.status(200).json( users );

    res.contentType = 'json'; // SEE  http://restify.com/docs/response-api/#json
    res.send( { users } );
    return next();

  } )
  .catch( ( err ) => {
    console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    // res.status(400).json( { errormessage: "Internal Error -- problem loading data." } );

    res.contentType = 'json'; // SEE  http://restify.com/docs/response-api/#json
    res.send( { errormessage: "Internal Error -- problem loading data." } );
  } );

  // TEST WITH...
  // http://127.0.0.1:5000/users/3

  // SOURCE...
  // https://sequelize.org/master/manual/model-querying-basics.html#the-basics
  // https://sequelize.org/master/manual/model-querying-basics.html#applying-where-clauses

} );

connectDB()
.then( async () => {
  await app.listen( PORT, HOSTNAME, () => {
    console.log( `http server running at http://${HOSTNAME}:${PORT}/` );
  } );
} );