// =============================================
// DATABASE SETUP 
// QUERY MODIFIERS
// https://sequelize.org/master/manual/getting-started.html#tip-for-reading-the-docs
// =============================================

const { Sequelize } = require("sequelize");

// =============================================
// DB CONFIG
// https://sequelize.org/master/manual/getting-started.html#connecting-to-a-database
// https://sequelize.org/master/manual/getting-started.html#logging
// https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor
// =============================================

const path = require('path');

const configurationObj = {
  dialect: "sqlite"
  // , storage: "./teststooges.sqlite3"
  // , storage: path.join( __dirname, "databases", "teststooges.sqlite3" )
  , storage: path.join( "./", "databases", "teststooges.sqlite3" )
};

const sequelize = new Sequelize( configurationObj );

// console.log( ">>>>>> sequelize \n\n", sequelize );

module.exports = sequelize;