// THIS WILL BORK SPECTACULARLY!!! 

if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { 
  PORT
  , HOSTNAME
  , USE_RESTIFY
} = process.env;

const { 
  Sequelize , DataTypes
  // , Op 
} = require("sequelize");

const epilogue = require("epilogue");
// const ForbiddenError = require("epilogue").Errors.ForbiddenError;

const http = require("http");

const database = new Sequelize( {
  dialect: "sqlite"
  , storage: "./teststooges.sqlite3"
} ); 

const User = database.define( "users" , {
  username: {
    type: DataTypes.STRING
    , allowNull: false
  }
  , birthday: {
    type: DataTypes.DATE
  }
  , tagline: {
    type: DataTypes.STRING
    , allowNull: false
  }
} );

// Initialize server
var server;
var app;

if( USE_RESTIFY ) {

  const restify = require("restify");

  app = server = restify.createServer();

  server.use( restify.plugins.acceptParser( server.acceptable ) );
  server.use( restify.plugins.queryParser() );
  server.use( restify.plugins.bodyParser() ); 
}
else {
  const express = require("express");
  const app = express();
  
  app.use( express.json() ); // for parsing application/json
  app.use( express.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded

  server = http.createServer( app );
}

// Initialize finale
epilogue.initialize( {
  app: app
  , sequelize: database 
} );

// Create REST resource
const resourceConfig = {
  model: User
  , endpoints: [ "/users" , "/users/:id" ]
};
const userResource = epilogue.resource( resourceConfig );

database
.sync()
/* .then( () => {
  server.listen( () => {
    const host = server.address().address;
    const port = server.address().port;
    console.log( 'Listening at URI %s on PORT %s', host, port );
  } );
} ) */
.then( async () => {
  await server.listen( PORT, HOSTNAME, () => {
    console.log( `http server running at http://${HOSTNAME}:${PORT}/` );
  } );
} );


/*
On the server we now have the following controllers and endpoints:
Controller  Endpoint  Description
userResource.create   POST /users   Create a user
userResource.list   GET /users  Get a listing of users
userResource.read   GET /users/:id  Get details about a user
userResource.update   PUT /users/:id  Update a user
userResource.delete   DELETE /users/:id   Delete a user
*/