const sequelize = require("./../custom_db/test-database-obj");
const User = require("./../custom_models_sequelize/user.test-model");

// =============================================
// SEEDER DATA
// =============================================
const seederData = [
  { 
    username: "Moe Howard"
    , birthday: new Date('June 19, 1897 03:24:00')
    , tagline: "Spread out!!!"
  }
  , { 
    username: "Larry Fine"
    , birthday: new Date('1902-10-05T03:24:00')
    , tagline: "Moe! Shemp! I can't see!!! Ha-ha!!! I had my eyes closed."
  }
  , { 
    username: "Curly Howard"
    , birthday: new Date(1903, 10, 22, 3, 24, 0)
    , tagline: "Whoop, whoop, whoop!!!"
  }
  , { 
    username: "Shemp Howard"
    , birthday: new Date(1895, 3, 11, 3, 24, 0)
    , tagline: "Why... YOU!"
  }
];

// "Shemp Howard", "March 11, 1895", "Why... YOU!"

// quickly convert vanilla text to escaped characters at 
// either https://www.w3schools.com/jsref/jsref_escape.asp
// or https://www.w3schools.com/jsref/jsref_encodeuri.asp

const seed = async () => {
  
  // create the table(s), dropping [them] first if [they] already existed
  // await sequelize.sync( { force: true } );

  // check the current state of the table(s) in the database (which columns [they] have, what are their data types, etc.), then, perform the necessary changes in the table(s) to make [them] match the model
  // await sequelize.sync({ alter: true });

  // create the table(s) if [they] don't exist (and do nothing if [they] already exist)
  await sequelize.sync();
  
  console.log(">>>>> All models were synchronized!\n\n");

  try {
    await User.bulkCreate( seederData );
    console.log( ">>>>> DB has been seeded!\n\n" );
  }
  catch (error) {
    console.error( "Failed to seed: \n\n" , error )
    await sequelize.close();
  }

};

seed();

// =============================================
// TEST THE DATABASE IN A SEPARATE TERMINAL WINDOW
// =============================================
/*
cd ~/delta/devc/react/v-sixteen-plus/fb-v3-cra/scotch-blogtracker-sqeprn-demo/
sqlite3 teststooges.sqlite3
.show
.headers ON
.mode column
SELECT * FROM users;
.exit  
*/