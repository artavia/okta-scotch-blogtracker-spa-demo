// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// process.env SETUP
// boilerplate
// JWT CONFIGURATION
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const {   
  OKTA_ORG_URL
  , OKTA_CLIENT_ID
} = process.env;

const OktaJwtVerifier = require("@okta/jwt-verifier");  // https://www.npmjs.com/package/@okta/jwt-verifier

const jwtConfig = {
  clientId: `${OKTA_CLIENT_ID}`,
  issuer: `${OKTA_ORG_URL}/oauth2/default` // required
};
const oktaJwtVerifier = new OktaJwtVerifier( jwtConfig );

// =============================================
// USE JWT VERSION ONE 
// from... https://developer.okta.com/docs/guides/protect-your-api/nodeexpress/require-authentication/
// =============================================
const authenticationRequired = ( req, res, next ) => {
  
  const authHeader = req.headers.authorization;
  
  if( typeof authHeader !== "undefined" ){
    
    const accessToken = req.headers.authorization.split(" ")[1];

    // "api://default" is called the "expectedAud" and corresponds with the jwtConfig.issuer parameter
    return oktaJwtVerifier.verifyAccessToken( accessToken, "api://default" )
    .then( ( jwt ) => {

      req.jwt = jwt;
      res.locals.jwt = jwt;

      next();
    } )
    .catch( ( err ) => {
      // res.status(401).send( err.message );
      next( err.message );
    } );
  }
  else {
    res.status(403); // apply Forbidden 403 if header is undefined
    let derrpage = new Error("Authorization header is required.");
    return next( derrpage );
  }
  
};

// =============================================
// USE JWT VERSION TWO
// from... https://developer.okta.com/blog/2018/07/10/build-a-basic-crud-app-with-node-and-react
// =============================================
const tryCatchAuthorization = async ( req, res, next ) => {
  
  try{
    const authHeader = req.headers.authorization;
    if( typeof authHeader === "undefined" ){
      res.status(403); // apply Forbidden 403 if header is undefined
      let derrpage = new Error("Authorization header is required.");
      return next( derrpage );
    }

    const accessToken = req.headers.authorization.split(" ")[1];

    // "api://default" is called the "expectedAud" and corresponds with the jwtConfig.issuer parameter
    let jwt = await oktaJwtVerifier.verifyAccessToken( accessToken, "api://default" );

    req.jwt = await jwt;
    res.locals.jwt = await jwt;

    next();
  }
  catch (error) {
    // res.status(401).send( error.message );
    next( error.message );
  } 
  
}; 

module.exports = { 
  authenticationRequired
  , tryCatchAuthorization 
};