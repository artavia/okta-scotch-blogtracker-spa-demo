// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP
// Establish the NODE_ENV toggle settings
// =============================================
const {   
  NODE_ENV
  , PORT
  , HOSTNAME
  // , OKTA_ORG_URL
  // , OKTA_CLIENT_ID
} = process.env;

const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production'; // true
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development'; // true

// =============================================
// BASE SETUP
// =============================================

const path = require("path");
const express = require("express");

const app = express();

// =============================================
// boilerplate
// =============================================
const cors = require("cors");

const finale = require("finale-rest");
// const ForbiddenError = require("finale-rest").Errors.ForbiddenError;

// =============================================
// IMPORT JWT MIDDLEWARE -- VERSIONS ONE AND TWO
// =============================================
const { authenticationRequired } = require("./custom_middleware/oktaJwtVerifier");
// const { tryCatchAuthorization } = require("./custom_middleware/oktaJwtVerifier");

// =============================================
// IMPORT DB MIDDLEWARE
// =============================================
const database = require("./custom_db/database-obj");
const connectDB = require("./custom_auth_promise/authentication");
const Post = require("./custom_models_sequelize/post.model");

// =============================================
// middleware - FINER DETAILS
// =============================================
app.use( express.json() ); // for parsing application/json
app.use( express.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded
app.use( cors() );


/* 
// =============================================
// this is for testing the build output from the desktop
// IN THE EVENT THAT I EVER SEE THIS ERROR DURING SIGN IN WITH THE REACT BUILD OUTPUT:
// "There was an unexpected internal error. Please try again."
// And, in essence, I want to stop any instances of "Cross-Origin Request Blocked"
// I need to consult the OKTA API ONLINE then go to API > Trusted Origins 
// Then, I add "Custom Okta Spa Demo Build Test... http://localhost:5000/... BOTH cors/redirect"
// =============================================
*/


// =============================================
// middleware - JWT RELATED - versions one and two
// https://expressjs.com/en/guide/routing.html
// app.all(), used to load middleware functions at a path for all HTTP request methods
// for the BUILD a blanket applicationto the routes with "*" is just TOO heavy-handed in scope
// the same applies to applying app.use INSTEAD OF SOMEROUTER.use( some_custom_middleware )
// =============================================

// app.use( authenticationRequired ); // all methods... the whole flippin' app
// app.use( tryCatchAuthorization );  // all methods... the whole flippin' app

// app.all( "*", authenticationRequired ); // all methods... all routes
// app.all( "*", tryCatchAuthorization );  // all methods... all routes

app.all( "/posts", authenticationRequired );   // all methods... the "/posts" route
app.all( "/posts/:id", authenticationRequired );   // all methods... the "/posts" route
// app.all( "/posts", tryCatchAuthorization ); // all methods... the "/posts" route
// app.all( "/posts/:id", tryCatchAuthorization ); // all methods... the "/posts" route

// =============================================
// INITIAL ROUTES - if any are necessary
// =============================================
app.get( "/blogpreview", (req, res, next) => {
  // res.status(200).json( {"message": "Konichiwa, guilas!"} );
  // res.contentType = 'json'; // SEE  http://restify.com/docs/response-api/#json
  // res.send( { message: "This is the Posts ENDPOINT." } );

  // TODO: retrieve all records
    
  Post.findAll()
  .then( posts => {
    
    // console.log( '>>>>>> All record: ' , posts );
    res.contentType = 'json'; 
    res.send( { posts } );
    return next();

  } )
  .catch( ( err ) => {
    // console.log( `There was derrpage: ` , JSON.stringify( err, null, 2 ) );
    res.contentType = 'json'; 
    res.send( { errormessage: "Internal Error -- problem loading data." } );

  } );

  // TEST WITH...
  // http://127.0.0.1:5000/blogpreview/
} );


// =============================================
// INITIALIZE AND CONFIGURE FINALE-REST END-POINTS
// Initialize finale
// Create REST resource
// SECURE FINALE-REST GENERATED ROUTES WITH JWT

/* On the server we WILL have the following controllers and endpoints:
Controller            Endpoint             Description
PostResource.create   POST /posts          Create a user
PostResource.list     GET /posts           Get a listing of posts
PostResource.read     GET /posts/:id       Get details about a user
PostResource.update   PUT /posts/:id       Update a user
PostResource.delete   DELETE /posts/:id    Delete a user */

// =============================================

const initializeConfig = {
  app: app
  , sequelize: database
};
finale.initialize( initializeConfig );

const resourceConfig = {
  model: Post
  , endpoints: [ "/posts" , "/posts/:id" ]
};
const PostResource = finale.resource( resourceConfig );


// =============================================
// BLANKET APPLICATION OF AUTHORIZATION MIDDLEWARE ON THE BACKEND TO ALL ROUTES
// SIMILAR TO WHAT IS SHOWN IN THE LESSON... SEE https://scotch.io/tutorials/build-a-blog-using-expressjs-and-react-in-30-minutes
// FOR ALL ENDPOINTS... SEE https://www.npmjs.com/package/finale-rest#protecting-finale-rest-endpoints
// =============================================

/* 
const allRoutesPromiseObject = (req, res, context) => { 
  return new Promise( (resolve,reject) => {

    // if(!req.user){ // undefined
    // if( !req.isAuthenticated() ){  // req.isAuthenticated is not a function
    // if(!req.userContext){ // undefined
    
    if(!req.jwt){
      console.log( "\n\n>>>>> req.jwt DOES NOT exist, ergot, I AM PRECLUDED from the finale-rest routes!!!\n\n"  ); 
      
      res.status(401);
      resolve( context.stop );
      
    }
    else { 
      // console.log( ">>>>> req.jwt \n\n " , req.jwt ); 
      console.log( "\n\n>>>>> req.jwt exists, ergot, I AM NOT precluded from the finale-rest routes!!!\n\n"  ); 
      resolve( context.continue );
    }
  } );
};

PostResource.all.auth( allRoutesPromiseObject ); 
*/


// =============================================
// PIECEMEAL APPLICATION OF AUTHORIZATION MIDDLEWARE ON THE BACKEND TO INDIVIDUAL ROUTES
// FOR SPECIFIC ENDPOINTS... SEE https://www.npmjs.com/package/finale-rest#controllers-and-endpoints
// =============================================

/**/ 
const piecemealPromiseObject = (req, res, context) => { 
  return new Promise( (resolve,reject) => {
    
    if(!req.jwt){
      console.log( "\n\n>>>>> req.jwt DOES NOT exist, ergot, You are Unauthorized from visiting this route!!!\n\n"  );
      
      // EITHER THIS
      throw new ForbiddenError("can't delete a user");
      
      // OR THIS... I use reject and not resolve at my own risk
      // reject( context.error(401, "You are Unauthorized!") );
    }
    else { 
      // console.log( ">>>>> req.jwt \n\n " , req.jwt ); 
      console.log( "\n\n>>>>> req.jwt exists, ergot, I CAN visit this route!!!\n\n"  );
      resolve( context.continue );
    }
  } );
};

PostResource.create.auth( piecemealPromiseObject ); // POST /posts
PostResource.list.auth( piecemealPromiseObject ); // GET /posts
PostResource.read.auth( piecemealPromiseObject ); // GET /posts/:id
PostResource.update.auth( piecemealPromiseObject ); // PUT /posts/:id 
PostResource.delete.auth( piecemealPromiseObject ); // DELETE /posts/:id 
/**/


// =============================================
// Serve static files from the React app
// Be sure that NODE_ENV is configured to "production" in the server dot env file
// or else "Error: ENOENT:" will show up in the server-side terminal
// =============================================
if( (IS_PRODUCTION_NODE_ENV === true) ){
  app.use( express.static( path.join( __dirname, 'public' ) ) ); 
}

// =============================================
// The "catchall" handler that will handle all requests in order to return React's index.html file.
// Be sure that NODE_ENV is configured to "production" in the server dot env file
// or else "Error: ENOENT:" will show up in the server-side terminal
// =============================================
const catchAllGET = ( req, res, next ) => {
  res.sendFile( path.join( __dirname , 'public', 'index.html' ) );
};
if( (IS_PRODUCTION_NODE_ENV === true) ){
  app.get( '*' , catchAllGET ); 
}

// =============================================
// DB and BACKEND SERVERS INSTANTIATION
// =============================================
connectDB()
.then( async () => {
  
  app.listen( PORT, HOSTNAME, () => { 
    
    // console.log( `Backend is running.`);
    // console.log( "NODE_ENV" , NODE_ENV );
  
    if( IS_PRODUCTION_NODE_ENV === true ){
      console.log( `Backend is running, baby.`);
    }
    if( IS_DEVELOPMENT_NODE_ENV === true ){
      console.log( `Production backend has started at http://${HOSTNAME}:${PORT} .`);
    }
    if(IS_PRODUCTION_NODE_ENV === false && IS_DEVELOPMENT_NODE_ENV === false ){ 
      console.log(`Development backend is now testing at port ${PORT} . Run and visit your frontend!`);
    }
  
  } );

} );