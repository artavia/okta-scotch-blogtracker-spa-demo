const sequelize = require("./../custom_db/database-obj");

const Post = require("./../custom_models_sequelize/post.model");

// =============================================
// SEEDER DATA
// =============================================

const postSeederData = [
  {
    title: "Star Wars-- A New Hope"
    , content: "A long time ago, in a galaxy far away... no Trekkies existed!"
  }
  , {
    title: "Jimmy Changa-- The Untold Story"
    , content: "The official documentary and true story behind the successful restaurant chain and the humble beginnings of a hard-working family of Puerto Rican immigrants from the streets of Trenton, New Jersey that took their homemade chimichangas recipe and grew it into an up and coming franchise empire. Move aside Tortilla Boy because here comes Jimmy Changa."
  }
  , {
    title: "The Rise and Fall of Roberto \"Babaloo\" Kebab"
    , content: "The unofficial documentary of how a Cuban/Lebanese family took their homemade recipes to catapult themselves to the level of international renown to only be brought down during the 1980s because of scandal involving circus carnies, Harvard University faculty, and a whole slew of other players never publicized until now."
  }
  , {
    title: "The Grinch is Dead"
    , content: "Because if the Grinch existed, then, it is about time that buffoon has encountered a final reckoning."
  }
  , {
    title: "Monkey Shines"
    , content: "Lancelot Link returns for the first time in over 50 years in his stunning big-screen debut nationwide. Watch as he and his gang of super sleuths go against the CHUMP crime syndicate. Original score and soundtrack will be performed by the Evolution Revolution."
  }
  , {
    title: "I Love You..."
    , content: "That's so sweet. I love me, too!"
  }
  , {
    title: "Fuzzy Wuzzy"
    , content: "The most famous animal to go out in public commando-style since Donald Duck."
  }  
  
];



// quickly convert vanilla text to escaped characters at 
// either https://www.w3schools.com/jsref/jsref_escape.asp
// or https://www.w3schools.com/jsref/jsref_encodeuri.asp

const seed = async () => {
  
  // create the table(s), dropping [them] first if [they] already existed
  // await sequelize.sync( { force: true } );

  // check the current state of the table(s) in the database (which columns [they] have, what are their data types, etc.), then, perform the necessary changes in the table(s) to make [them] match the model
  // await sequelize.sync({ alter: true });

  // create the table(s) if [they] don't exist (and do nothing if [they] already exist)
  await sequelize.sync();
  
  console.log(">>>>> All models were synchronized!\n\n");

  try {
    await Post.bulkCreate( postSeederData );
    console.log( ">>>>> DB has been seeded with the blogposts table!\n\n" );
  }
  catch (error) {
    console.error( "Failed to seed: \n\n" , error )
    await sequelize.close();
  }

};

seed();

// =============================================
// TEST THE DATABASE IN A SEPARATE TERMINAL WINDOW
// =============================================
/*

cd {PROJECT_ROOT}/

sqlite3 server/databases/blogposts.sqlite3
.show
.headers ON
.mode column
SELECT * FROM stooges;
SELECT * FROM posts;
.exit  
*/