const sequelize = require("./../custom_db/database-obj")

const { DataTypes } = require("sequelize");
// const { Sequelize } = require("sequelize");

console.log( "Let's get this show on the road!!!\n\n" );

// =============================================
// MODEL CREATION PHASE FOR MAPPING
// https://sequelize.org/master/manual/model-basics.html#model-definition
// =============================================

const Post = sequelize.define( "posts" , {
  title: {
    type: DataTypes.STRING
    , allowNull: false
  }
  , content: {
    type: DataTypes.TEXT
    , allowNull: false
  }
} );

module.exports = Post;