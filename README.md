# Okta Scotch Blogtracker Spa Demo

## Description
This is a more in&#45;depth re&#45;expression of a write&#45;up found at scotch.io by Macy Ngan.

### Processing and/or completion date(s)
  - July 06, 2020 to July 11, 2020: Re-expressed the project then discovered that I essentially built a house without a front door...
  - July 11, 2020 to July 18, 2020: There were additional constraints to address which, in turn, required additional research and sifting through the muck.

## It is finished
There are a few additional steps that could be taken but by and large it is done&hellip; and it is bullet resistant! You now have a forward&#45;ready boilerplate that can easily be configured for use with MySql instead of SQLite3.

## Based on an article found at scotch.io
In 2019, Macy Ngan Toptal wrote [Build a Blog Using Express.js and React in 30 Minutes](https://scotch.io/tutorials/build-a-blog-using-expressjs-and-react-in-30-minutes "link to scotch.io"). I improved on it wanted to share the lessons learned. I used CreateReactApp, thus, it changes the architecture of each the server and the front&#45;end sides significantly. I hope you enjoy it.

## Necessary package replacements, insertions, and deletions
Some of the packages that come to mind which didn&rsquo;t make the cut are listed below:
  - epilogue
  - express&#45;session
  - body&#45;parser
  - @okta/oidc&#45;middleware@latest


Other packages that were utilized are listed, too:
  - finale&#45;rest
  - @okta/jwt&#45;verifier@latest
  - @okta/okta&#45;signin&#45;widget@latest
  - @okta/okta&#45;react@latest


## Component composition
Whereever possible, I converted class&#45;based components to their function&#45;based equivalents (which is just about everywhere).

## Practice Tests
I have included a folder called practice which is basically a warm&#45;up for navigating the back&#45;end end&#45;points.

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the 
[ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial") (or, **AFT** for short) on two different occasions. The authors **purported** that one does not need to know the other minor themes in order to complete the lesson. That would be nice if it were true, however, it was not meant to be so.

In terms of the **AFT**, it became obvious that some of the technologies were archaic, possibly deprecated &amp; likely containing security vulnerabilities. 

Therefore, in order to follow and ultimately complete the **AFT** to fruition, I decided to embark on learning and/or shoring&#45;up some of the minor themes involved such as Sqlite3 and Sequelize (which I had been meaning to do for quite some time). 

Embarking on this exercise permitted me to shore&#45;up my skills as far as minor and major themes are concerned (I refer to **finale&#45;rest** and **Sequelize**, respectively).

Originally, the problem was never with learning GraphQL and/or ApolloGraphQL. The bugs I was sure at the time were limited to (again) the minor themes. I will continue to publish and share programming knowledge as long as the cows don't come home. Personally, I think the plan has been a success so far and I am grateful to the Lord for this opportunity to learn and share. 

I thank you for your interest.

### God bless
Do not tarry do not wait. The time is now that you get right with God. I am not here to wag my finger at anybody. I am suggesting that each of you fornicators, liars, thieves, hypocrites and gaslighters are not totally a lost cause. Believe me-- I used to be one (take your pick)!

Work out your redemption with fear and trembling before the throne of God through his only begotten son, Jesus Christ. If you are an atheist or even a very nice &quot;religious&quot; person who by chance within the last year bought an ice cream cone for every child within a 100 mile radius for the past year but you don&rsquo;t know the love and embodiment of the word of God in Jesus Christ, then, these words apply to you, too. The Lord will know each one of us by our fruit! 

Both **1 John 4:7** and **1 John 4:8** state, respectively, that &quot;He that loves is of God. And, he that does not love is not of God.&quot; We all serve somebody whether it is God or our loins. None of us are without a master. Ask yourselves, who or what dominates you? I just want the same for all of you because this is not a joke.

A lot of you may hate me but I do not hate you back&hellip; and, that&rsquo;s the truth. I pray for my enemies rather than curse them.

May the grace of the Lord Jesus Christ be with you all.